package dev.jamesroberts.quickbreak.model;

import org.junit.Ignore;
import org.junit.Test;

import java.util.Calendar;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class AlarmListTest {
    @Test
    public void contains_whenElementIsInList_returnsTrue() {
        AlarmList list = new AlarmList();
        AlarmModel model = new AlarmModel(
                Calendar.getInstance(),
                Calendar.getInstance(),
                0,
                "sound",
                true);
        list.add(model);
        assertTrue(list.contains(model));
    }

    @Test
    public void contains_whenElementIsNotInList_returnsFalse() {
        AlarmList list = new AlarmList();
        AlarmModel model1 = new AlarmModel(
                Calendar.getInstance(),
                Calendar.getInstance(),
                0,
                "sound",
                true);
        AlarmModel model2 = new AlarmModel(
                Calendar.getInstance(),
                Calendar.getInstance(),
                0,
                "sound",
                true);
        list.add(model1);
        assertFalse(list.contains(model2));
    }

    @Test
    public void contains_whenElementIsRemovedFromList_returnsFalse() {
        AlarmList list = new AlarmList();
        AlarmModel model1 = new AlarmModel(
                Calendar.getInstance(),
                Calendar.getInstance(),
                0,
                "sound",
                true);
        AlarmModel model2 = new AlarmModel(
                Calendar.getInstance(),
                Calendar.getInstance(),
                0,
                "sound",
                true);
        list.add(model1);
        list.add(model2);
        list.remove(model2);
        assertFalse(list.contains(model2));
    }

    @Test
    public void get_whenElementIsInList_returnsAlarmModelWithProvidedId() {
        AlarmList list = new AlarmList();
        AlarmModel model1 = new AlarmModel(
                Calendar.getInstance(),
                Calendar.getInstance(),
                0,
                "sound",
                true);
        AlarmModel model2 = new AlarmModel(
                Calendar.getInstance(),
                Calendar.getInstance(),
                0,
                "sound",
                true);
        list.add(model1);
        list.add(model2);
        assertEquals(list.get(model1.getId()), model1);
        assertEquals(list.get(model2.getId()), model2);
    }

    @Test
    public void get_whenElementIsNotInList_returnsNull() {
        AlarmList list = new AlarmList();
        AlarmModel model1 = new AlarmModel(
                Calendar.getInstance(),
                Calendar.getInstance(),
                0,
                "sound",
                true);
        AlarmModel model2 = new AlarmModel(
                Calendar.getInstance(),
                Calendar.getInstance(),
                0,
                "sound",
                true);
        assertNull(list.get(model1.getId()));
        assertNull(list.get(model2.getId()));
    }

    @Test
    public void update_changesContentToNewValues() {
        AlarmList list = new AlarmList();
        AlarmModel model1 = new AlarmModel(
                1,
                Calendar.getInstance(),
                Calendar.getInstance(),
                0,
                "sound",
                true);
        AlarmModel model2 = new AlarmModel(
                1,
                Calendar.getInstance(),
                Calendar.getInstance(),
                10,
                "otherSound",
                true);
        list.add(model1);
        list.update(model2);
        assertEquals(list.get(model1.getId()).getAlarmSound(), "otherSound");
    }

}