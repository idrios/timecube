package dev.jamesroberts.quickbreak.model;

import org.junit.Test;

import java.util.Calendar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class AlarmModelTest {
    @Test
    public void constructor_whenProvidedArguments_generatesUniqueIds() {
        AlarmModel model1 = new AlarmModel(
                Calendar.getInstance(),
                Calendar.getInstance(),
                0,
                "sound",
                true);
        AlarmModel model2 = new AlarmModel(
                Calendar.getInstance(),
                Calendar.getInstance(),
                0,
                "sound",
                true);
        assertNotEquals(model1.getId(), model2.getId());
    }
}
