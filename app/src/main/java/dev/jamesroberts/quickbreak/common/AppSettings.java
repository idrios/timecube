package dev.jamesroberts.quickbreak.common;

/**
 * File for saving App Settings, which are global variables that should have customizeable info --
 * things like LogLevel if I ever implement that, or maximum number of alarms.
 *
 * I don't believe this is the standard way for implementing AppSettings though in Java/Android
 * so I should look into changing this in the future.
 *
 * Created by James Roberts, 26-Dec-2020
 */
public class AppSettings {
    // 20 is the maximum number of alarms because I only have 20 alarm id's available in
    // res/values/ids.xml
    public static int MAX_NUM_ALARMS = 20;
}
