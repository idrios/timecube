package dev.jamesroberts.quickbreak.memory;

import java.lang.reflect.Type;
import java.util.List;

import dev.jamesroberts.quickbreak.model.AlarmList;
import dev.jamesroberts.quickbreak.model.AlarmModel;

/**
 * Interface for data access object; abstracts whether memory is stored in private storage,
 * a local file, a local database, external database, or any other creative solution
 */
public interface AlarmDao {

    /**
     * Add a new alarm to data storage.
     * <p>
     * Return value is the operation status code. 0 indicates successful operation.
     *
     * @param  alarm  AlarmModel object for storing the data
     * @return      operation status code
     * @see         AlarmModel
     */
    public int addAlarm(AlarmModel alarm);

    /**
     * Get an alarm from data storage using its id. Returns null if operation failed
     *
     * @param  id  an integer id of an alarm to get from storage
     * @return      the AlarmModel object from storage
     * @see         AlarmModel
     */
    public AlarmModel getAlarmById(int id);

    /**
     * Get all alarms from data storage. Returns null if operation failed
     *
     * @return      a list of AlarmModel objects from storage
     * @see         AlarmModel
     */
    public AlarmList getAllAlarms();

    /**
     * Update an alarm in data storage.
     * <p>
     * Return value is the operation status code. 0 indicates successful operation.
     *
     * @param  id  an integer id of the alarm in storage being updated
     * @param alarm  new AlarmModel object to replace the values in the old one. *Null and falsy
     *               values do not overwrite*.
     * @return      operation status code
     * @see         AlarmModel
     */
    public int updateAlarm(int id, AlarmModel alarm);

    /**
     * Delete an alarm in data storage.
     * <p>
     * Return value is the operation status code. 0 indicates successful operation.
     *
     * @param  id  id of AlarmModel object being deleted from storage
     * @return      operation status code
     * @see         AlarmModel
     */
    public int deleteAlarm(int id);

}
