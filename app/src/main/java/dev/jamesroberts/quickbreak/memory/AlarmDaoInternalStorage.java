package dev.jamesroberts.quickbreak.memory;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import dev.jamesroberts.quickbreak.common.Global;
import dev.jamesroberts.quickbreak.model.AlarmList;
import dev.jamesroberts.quickbreak.model.AlarmModel;
import dev.jamesroberts.quickbreak.model.FrequencyType;

public class AlarmDaoInternalStorage implements AlarmDao {

    private static AlarmDaoInternalStorage mInstance;
    private static final String FILENAME = "timecube_alarms.bin";
    private AlarmDaoInternalStorage() {
    }

    public static AlarmDaoInternalStorage getInstance() {
        if(mInstance == null) {
            mInstance = new AlarmDaoInternalStorage();
        }
        return mInstance;
    }

    @Override
    public int addAlarm(AlarmModel alarm) {
        Context context = Global.APPLICATION_CONTEXT;
        File file = new File(context.getFilesDir().getAbsolutePath()+"/"+FILENAME);
        AlarmList savedList;

        // If file exists, load the list and add this alarm to that list
        if(file.exists()){
            try {
                FileInputStream fis = context.openFileInput(FILENAME);
                ObjectInputStream is = new ObjectInputStream(fis);
                savedList = (AlarmList)is.readObject();
            } catch (FileNotFoundException e) {
                System.err.println("File not found while trying to read existing file before adding alarm #"+alarm.getId());
                savedList = new AlarmList();
            } catch (IOException e) {
                System.err.println("IOException while trying to read existing file before adding alarm #"+alarm.getId());
                savedList = new AlarmList();
            } catch (ClassNotFoundException e){
                System.err.println("AlarmList not found while trying to read existing file before adding alarm #"+alarm.getId());
                savedList = new AlarmList();
            }
        }
        else {
            savedList = new AlarmList();
        }

        // Now write or overwrite the list with a new list that including the new alarm
        savedList.add(alarm);
        try {
            FileOutputStream fos = context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(savedList);
            os.close();
            fos.close();
        } catch (IOException e) {
            System.err.println("IOException while adding alarm #"+alarm.getId());
            return -1;
        }
        return 0;
    }

    @Override
    public AlarmModel getAlarmById(int id) {
        Context context = Global.APPLICATION_CONTEXT;
        File file = new File(context.getFilesDir().getAbsolutePath()+"/"+FILENAME);
        if(!file.exists()){
            return null;
        }
        try {
            FileInputStream fis = context.openFileInput(FILENAME);
            ObjectInputStream is = new ObjectInputStream(fis);
            AlarmList alarmList = (AlarmList)is.readObject();
            is.close();
            fis.close();
            return alarmList.get(id);
        } catch (FileNotFoundException e) {
            System.err.println("File not found while getting alarm #"+id);
        } catch (IOException e) {
            System.err.println("IO Exception getting alarm #"+id);
        } catch (ClassNotFoundException e) {
            System.err.println("No AlarmList found while getting alarm #"+id);
        }
        return null;
    }

    @Override
    public AlarmList getAllAlarms() {
        Context context = Global.APPLICATION_CONTEXT;
        File file = new File(context.getFilesDir().getAbsolutePath()+"/"+FILENAME);
        if(!file.exists()){
            return new AlarmList();
        }
        try {
            FileInputStream fis = context.openFileInput(FILENAME);
            ObjectInputStream is = new ObjectInputStream(fis);
            return (AlarmList) is.readObject();
        } catch (FileNotFoundException e){
            System.err.println("File not found while getting all alarms");
            return new AlarmList();
        } catch (IOException e) {
            System.err.println("IO Exception getting all alarms");
            return new AlarmList();
        } catch (ClassNotFoundException e) {
            System.err.println("AlarmList class type not found while getting all alarms");
            file.delete(); // There's something wrong with the file so delete it.
            return new AlarmList();
        }
    }

    @Override
    public int updateAlarm(int id, AlarmModel alarm) {
        Context context = Global.APPLICATION_CONTEXT;
        File file = new File(context.getFilesDir().getAbsolutePath()+"/"+FILENAME);
        AlarmList savedList;

        // If file exists, load the list. Else create a new list
        if(file.exists()){
            try {
                FileInputStream fis = context.openFileInput(FILENAME);
                ObjectInputStream is = new ObjectInputStream(fis);
                savedList = (AlarmList)is.readObject();
            } catch (FileNotFoundException e) {
                System.err.println("File not found while trying to read existing file before updating alarm #"+id);
                savedList = new AlarmList();
            } catch (IOException e) {
                System.err.println("IOException while trying to read existing file before updating alarm #"+id);
                savedList = new AlarmList();
            } catch (ClassNotFoundException e){
                System.err.println("AlarmList not found while trying to read existing file before updating alarm #"+id);
                savedList = new AlarmList();
            }
        }
        else {
            savedList = new AlarmList();
        }

        // If list contains the alarm, update with the new alarm values
        if(savedList.contains(id)){
            savedList.update(alarm);
        }
        // Otherwise, add a new alarm
        else {
            savedList.add(alarm);
        }

        // Now write or overwrite the list with a new list that including the new alarm
        try {
            FileOutputStream fos = context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(savedList);
            os.close();
            fos.close();
        } catch (IOException e) {
            System.err.println("IOException while updating alarm #"+id);
            return -1;
        }
        return 0;
    }

    @Override
    public int deleteAlarm(int id) {
        Context context = Global.APPLICATION_CONTEXT;
        File file = new File(context.getFilesDir().getAbsolutePath()+"/"+FILENAME);
        AlarmList savedList;

        // If file exists, load the list. Else create a new list
        if(file.exists()){
            try {
                FileInputStream fis = context.openFileInput(FILENAME);
                ObjectInputStream is = new ObjectInputStream(fis);
                savedList = (AlarmList)is.readObject();
            } catch (FileNotFoundException e) {
                System.err.println("File not found while trying to read existing file before deleting alarm #"+id);
                savedList = new AlarmList();
            } catch (IOException e) {
                System.err.println("IOException while trying to read existing file before deleting alarm #"+id);
                savedList = new AlarmList();
            } catch (ClassNotFoundException e){
                System.err.println("AlarmList not found while trying to read existing file before deleting alarm #"+id);
                savedList = new AlarmList();
            }
        }
        else {
            savedList = new AlarmList();
        }

        // If list contains the alarm, remove it
        if(savedList.contains(id)){
            savedList.remove(savedList.get(id));
        }
        // Otherwise, return and don't bother saving the list since we didn't change anything
        else {
            return 0;
        }

        // Now write or overwrite the list with a new list that no longer contains the alarm
        try {
            FileOutputStream fos = context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(savedList);
            os.close();
            fos.close();
        } catch (IOException e) {
            System.err.println("IOException while removing alarm #"+id);
            return -1;
        }
        return 0;

    }
}
