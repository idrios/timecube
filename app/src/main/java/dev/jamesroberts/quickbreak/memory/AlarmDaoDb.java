package dev.jamesroberts.quickbreak.memory;

import java.util.List;

import dev.jamesroberts.quickbreak.model.AlarmList;
import dev.jamesroberts.quickbreak.model.AlarmModel;

/**
 * Data Access Object to provide an interface for communicating with the AlarmDB
 */
public class AlarmDaoDb implements AlarmDao{

    // Singleton instance of this object
    private static AlarmDaoDb mInstance;

    public AlarmDaoDb() {
        // Empty constructor
    }

    public static AlarmDaoDb getInstance() {
        if(mInstance == null) {
            mInstance = new AlarmDaoDb();
        }
        return mInstance;
    }


    @Override
    public int addAlarm(AlarmModel alarm) {
        return 0;
    }

    @Override
    public AlarmModel getAlarmById(int id) {
        return new AlarmModel(){
            // Get by id stuff
        };
    }

    @Override
    public AlarmList getAllAlarms() {
        return null;
    }

    @Override
    public int updateAlarm(int id, AlarmModel alarm) {
        return 0;
    }

    @Override
    public int deleteAlarm(int id) {
        return 0;
    }
}
