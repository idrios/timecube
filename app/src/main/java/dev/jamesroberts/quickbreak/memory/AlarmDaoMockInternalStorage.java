package dev.jamesroberts.quickbreak.memory;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import dev.jamesroberts.quickbreak.model.AlarmList;
import dev.jamesroberts.quickbreak.model.AlarmModel;
import dev.jamesroberts.quickbreak.model.FrequencyType;

public class AlarmDaoMockInternalStorage implements AlarmDao {

    private static AlarmDaoMockInternalStorage mInstance;
    private AlarmDaoMockInternalStorage(){
    }

    @Override
    public int addAlarm(AlarmModel alarm) {
        return 0;
    }

    @Override
    public AlarmModel getAlarmById(int id) {
        return null;
    }

    public static AlarmDaoMockInternalStorage getInstance(){
        if(mInstance == null){
            mInstance = new AlarmDaoMockInternalStorage();
        }
        return mInstance;
    }

    @Override
    public AlarmList getAllAlarms() {
        AlarmList alarms = new AlarmList();

        Calendar time1 = new GregorianCalendar();
        time1.set(Calendar.HOUR, 1);
        time1.set(Calendar.MINUTE, 20);
        time1.set(Calendar.AM_PM, Calendar.PM);
        List<Integer> days1 = new ArrayList<>();
        days1.add(Calendar.SATURDAY);
        days1.add(Calendar.WEDNESDAY);
        days1.add(Calendar.THURSDAY);
        Calendar freq1 = new GregorianCalendar();
        freq1.set(Calendar.HOUR, 0);
        freq1.set(Calendar.MINUTE, 15);

        Calendar time2 = new GregorianCalendar();
        time2.set(Calendar.HOUR, 10);
        time2.set(Calendar.MINUTE, 0);
        time2.set(Calendar.AM_PM, Calendar.AM);
        List<Integer> days2 = new ArrayList<>();
        days2.add(Calendar.MONDAY);
        days2.add(Calendar.TUESDAY);
        days2.add(Calendar.THURSDAY);
        Calendar freq2 = new GregorianCalendar();
        freq2.set(Calendar.HOUR, 1);
        freq2.set(Calendar.MINUTE, 20);

        alarms.add(
                new AlarmModel(
                        time1,
                        days1,
                        FrequencyType.UNLIMITED,
                        freq1,
                        10,
                        10,
                        "wolfman",
                        true
                )
        );
        alarms.add(
                new AlarmModel(
                        time2,
                        days2,
                        FrequencyType.LIMITED,
                        freq2,
                        3,
                        3,
                        "dunno",
                        false
                )
        );

        return alarms;
    }

    @Override
    public int updateAlarm(int id, AlarmModel alarm) {
        return 0;
    }

    @Override
    public int deleteAlarm(int id) {
        return 0;
    }
}
