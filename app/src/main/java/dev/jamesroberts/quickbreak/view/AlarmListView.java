package dev.jamesroberts.quickbreak.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import dev.jamesroberts.quickbreak.common.AppSettings;
import dev.jamesroberts.quickbreak.common.Global;
import dev.jamesroberts.quickbreak.model.AlarmModel;
import dev.jamesroberts.quickbreak.service.AlarmEventObserver;
import dev.jamesroberts.quickbreak.service.AlarmManager;

/**
 * Custom view to contain the list of alarms with logic for adding and removing
 */
public class AlarmListView extends LinearLayout implements AlarmEventObserver {

    public AlarmListView(Context context) {
        super(context);
    }

    public AlarmListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    // Called when view is attached to the window, listener methods recommended to go here
    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        AlarmManager.getInstance().subscribe(this);
    }

    // Called when view is removed from the window, cleanup methods recommended to go here
    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        AlarmManager.getInstance().unsubscribe(this);
    }

    // Called when all children have been added
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
    }

    public void loadAlarmListFromMemory() {
        for(AlarmModel alarmModel : AlarmManager.getInstance().getAlarmList()) {
            onAddAlarm(alarmModel);
        }
    }

    @Override
    public void onAddAlarm(AlarmModel alarm) {
        if(alarm.getId() > AppSettings.MAX_NUM_ALARMS) {
            System.err.println("Too many alarms");
            return;
        }
        AlarmView alarmView = AlarmView.fromXML(getContext(), this);
        alarmView.setModel(alarm);
        int rId = getResources().getIdentifier("alarm_model"+alarm.getId(), "ids", Global.PACKAGE_NAME); // R.id.alarm_modelX for variable X
        alarmView.setId(rId); // give the alarm view resource id so we can find it whenever we need
        addView(alarmView, Math.max(0, this.getChildCount()-1)); // add the view as the last element in the linear layout
    }

    @Override
    public void onUpdateAlarm(AlarmModel alarm) {
        int rId = getResources().getIdentifier("alarm_model"+alarm.getId(), "ids", Global.PACKAGE_NAME); // R.id.alarm_modelX for variable X
        AlarmView alarmView = (AlarmView) findViewById(rId); // find the alarm view in the list
        alarmView.setModel(alarm); // use the updated alarm as this alarm view's reference model
    }

    @Override
    public void onRemoveAlarm(AlarmModel alarm) {
        int rId = getResources().getIdentifier("alarm_model"+alarm.getId(), "ids", Global.PACKAGE_NAME); // R.id.alarm_modelX for variable X
        AlarmView alarmView = (AlarmView) findViewById(rId); // find the alarm view in the list
        removeView(alarmView); // remove the alarm
    }
}
