package dev.jamesroberts.quickbreak.view.alarmeditorviewsupport;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ToggleButton;

import dev.jamesroberts.quickbreak.R;

/**
 * Section of the Alarm Editor for enabling or disabling the alarm altogether.
 * This section didn't really need its own view but I did it for consistency
 *
 * Created by James Roberts, 1-Dec-2020
 */
public class EnableView extends FrameLayout {

    private ToggleButton mToggleEnable;

    public EnableView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutInflater.inflate(R.layout.view_enable, this);
        mToggleEnable = findViewById(R.id.toggle_alarm_enable);
    }

    public boolean getEnabled() {
        return mToggleEnable.isChecked();
    }

    public void setEnabled(boolean enabled){
        mToggleEnable.setChecked(enabled);
    }
}
