package dev.jamesroberts.quickbreak.view.alarmeditorviewsupport.clockviewsupport;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import dev.jamesroberts.quickbreak.R;

/**
 * Adapter class for DiscreteViewHolder, per recommendations set by
 * https://github.com/yarolegovich/DiscreteScrollView
 *
 * Created by James Roberts, 27-Nov-2020
 */
public class TimeCollectionAdapter extends RecyclerView.Adapter<TimeCollectionAdapter.ViewHolder> {
    private List<TimeElement> timeList;

    public TimeCollectionAdapter(List<TimeElement> timeList) {
        this.timeList = timeList;
    }

    @NonNull
    @Override
    public TimeCollectionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.view_time_selector_card, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull TimeCollectionAdapter.ViewHolder holder, int position) {
        holder.text.setText(timeList.get(position).getValue());
    }

    @Override
    public int getItemCount() {
        return timeList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView text;

        public ViewHolder(View itemView){
            super(itemView);
            text = itemView.findViewById(R.id.time_element_text);
        }
    }
}
