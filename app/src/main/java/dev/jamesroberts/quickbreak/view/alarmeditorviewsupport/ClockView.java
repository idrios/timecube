package dev.jamesroberts.quickbreak.view.alarmeditorviewsupport;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import java.util.Calendar;
import java.util.GregorianCalendar;

import dev.jamesroberts.quickbreak.R;
import dev.jamesroberts.quickbreak.view.alarmeditorviewsupport.clockviewsupport.TimeView;

/**
 * ClockView is the View for displaying time on the UI.
 *
 * This uses 3 DiscreteScrollViews to let the user pick hour, minute, and am/pm. It is the equivalent
 * of shop activity in
 * https://github.com/yarolegovich/DiscreteScrollView/blob/master/sample/src/main/java/com/yarolegovich/discretescrollview/sample/shop/ShopActivity.java
 * with minor changes because this is a view and not a full activity
 *
 * Created by James Roberts, 1-Nov-2020
 */
public class ClockView extends LinearLayout  {

    private TimeView mHourPicker;
    private TimeView mMinutePicker;
    private TimeView mAmpmPicker;

    public ClockView(Context context) {
        super(context);
    }

    public ClockView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutInflater.inflate(R.layout.view_clock, this);
    }

    // onFinishInflate is called when all child elements have been rendered. I was worried about
    // possible race conditions like a user being able to see the empty time-pickers after
    // super.onFinishInflate but before loading values, but that's not an issue because things
    // don't get rendered until a later step of the View lifecycle "OnDraw"
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        String[] hours = new String[]{
                "1","2","3","4","5","6","7","8","9","10","11","12"
        };
        String[] minutes = new String[]{
                "00", "01", "02", "03", "04", "05", "06", "07", "08", "09",
                "10", "11", "12", "13", "14", "15", "16", "17", "18", "19",
                "20", "21", "22", "23", "24", "25", "26", "27", "28", "29",
                "30", "31", "32", "33", "34", "35", "36", "37", "38", "39",
                "40", "41", "42", "43", "44", "45", "46", "47", "48", "49",
                "50", "51", "52", "53", "54", "55", "56", "57", "58", "59",
        };
        String[] ampm = new String[]{
                "AM", "PM"
        };
        mHourPicker = findViewById(R.id.hour_picker);
        mMinutePicker = findViewById(R.id.minute_picker);
        mAmpmPicker = findViewById(R.id.ampm_picker);

        mHourPicker.loadValues(hours);
        mMinutePicker.loadValues(minutes);
        mAmpmPicker.loadValues(ampm);
    }

    // Return Calendar instead of Date or Time or DateTime or LocalDateTime because it's the only
    // friggin class that works consistently across Android versions without needing to do
    // confusing things with gradle)
    public Calendar getTime(){
        int hour = Integer.parseInt(mHourPicker.getTime());
        int minute = Integer.parseInt(mMinutePicker.getTime());
        int ampm = mAmpmPicker.getTime()
                .equals("AM")
                ? Calendar.AM
                : Calendar.PM;
        Calendar time = new GregorianCalendar();
        time.set(Calendar.HOUR, hour);
        time.set(Calendar.MINUTE, minute);
        time.set(Calendar.AM_PM, ampm);
        return time;
    }

    public void setTime(Calendar time){
        String hour = String.valueOf(time.get(Calendar.HOUR));
        String minute = String.valueOf(time.get(Calendar.MINUTE));
        if(minute.length()==1){ // Fix when minute is returned as, e.g. "5" instead of "05"
            minute="0"+minute;
        }
        String ampm = time.get(Calendar.AM_PM)==Calendar.AM
                ? "AM"
                : "PM";
        System.out.println("Setting time to "+hour+":"+minute+" "+ampm);
        mHourPicker.setToValue(hour);
        mMinutePicker.setToValue(minute);
        mAmpmPicker.setToValue(ampm);
    }
}