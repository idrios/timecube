package dev.jamesroberts.quickbreak.view.alarmeditorviewsupport.clockviewsupport;

/**
 * The element to be displayed in the custom clock. Will be a number between 1 to 12 if
 * its for the hour position, 0 to 59 if it's for the minute, or AM or PM
 *
 * Created by James Roberts on 26-Nov-2020
 */
public class TimeElement {
    private final String value;

    public TimeElement(String value) {
        this.value = value;
    }

    public String getValue(){
        return this.value;
    }

}
