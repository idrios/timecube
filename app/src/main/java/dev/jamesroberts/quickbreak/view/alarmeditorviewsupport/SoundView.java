package dev.jamesroberts.quickbreak.view.alarmeditorviewsupport;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;

import dev.jamesroberts.quickbreak.R;

/**
 * Section of the Alarm Editor for selecting an audio file to play back as the alarm.
 *
 * The Spinner needs an array adapter the same way the clockview needed one, but because this one
 * was simple I was at least able to do the simplest version of ArrayAdapter I could find. Follow
 * tutorials for android spinners to understand what's happening here
 *
 * Created by James Roberts, 1-Dec-2020
 */
public class SoundView extends LinearLayout implements AdapterView.OnItemSelectedListener {

    private String mCurrentSound;
    private Spinner mSoundPicker;
    private ArrayAdapter<CharSequence> mSoundAdapter;

    public SoundView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutInflater.inflate(R.layout.view_sound, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mSoundPicker = findViewById(R.id.sound_picker);
        //SpinnerAdapter adapter = ArrayAdapter.createFromResource(); // Possibly use this to retrieve from memory
        mSoundAdapter = new ArrayAdapter<CharSequence>(getContext(), R.layout.support_simple_spinner_dropdown_item); /* wat where did this layout file come from */
        mSoundAdapter.add("default.sound");
        mSoundAdapter.add("dog.mp3");
        mSoundAdapter.add("Stairway to Heaven.mp3");
        mSoundAdapter.add("Baby Laughing Ha Ha.ogg");
        mSoundAdapter.add("woof.ogg");
        mSoundAdapter.add("beep.ogg");
        mSoundAdapter.add("beep but louder.ogg");
        mSoundAdapter.add("Siren horn wake me the heck up!!.ogg");
        mSoundAdapter.add("This one is specifically to be the longest title I could possibly come up with (though probably I should set a max length specifically to avoid this?)");
        mSoundAdapter.add("Jerj Clooners.ogg");
        mSoundAdapter.add("Sandwich.mp3");
        mSoundAdapter.add("National Anthem.mp3");
        mSoundAdapter.add("mp3.mp4 HAX");
        mSoundAdapter.add("vibrate-only");
        mSoundAdapter.add("silence.jpeg");
        mSoundAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        mSoundPicker.setAdapter(mSoundAdapter);
        mSoundPicker.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        mCurrentSound = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public String getSound() {
        return mCurrentSound;
    }

    public void setSound(String sound) {
        mSoundPicker.setSelection(mSoundAdapter.getPosition(sound));
    }
}
