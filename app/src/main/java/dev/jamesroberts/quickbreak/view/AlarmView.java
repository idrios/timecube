package dev.jamesroberts.quickbreak.view;

import android.content.Context;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.FragmentManager;

import java.util.Calendar;
import java.util.Date;

import dev.jamesroberts.quickbreak.R;
import dev.jamesroberts.quickbreak.fragment.AlarmEditorDialogFragment;
import dev.jamesroberts.quickbreak.model.AlarmModel;
import dev.jamesroberts.quickbreak.service.AlarmEventObserver;

/**
 * UI view of AlarmModel
 */
public class AlarmView extends LinearLayout {

    private AlarmModel mAlarmModel;
    private SwitchCompat mSwitchAlarmEnable;
    private TextView mTextAlarmDefinitionInfo;
    private TextView mTextAlarmInstanceInfo;

    public AlarmView(Context context) {
        super(context);
    }

    public AlarmView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public static AlarmView fromXML(Context context, ViewGroup parent) {
        return (AlarmView) LayoutInflater.from(context).inflate(R.layout.view_alarm, parent, false);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        this.mSwitchAlarmEnable = (SwitchCompat)findViewById(R.id.switchAlarmEnable);
        this.mTextAlarmDefinitionInfo = (TextView)findViewById(R.id.textAlarmDefinitionInfo);
        this.mTextAlarmInstanceInfo = (TextView)findViewById(R.id.textAlarmInstanceInfo);

        mSwitchAlarmEnable.setOnCheckedChangeListener( new SwitchCompat.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                toggleEnable(isChecked);
            }
        });
        this.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doAlarmEditorDialog();
            }
        });

    }

    public AlarmModel getModel(){
        return this.mAlarmModel;
    }

    public void setModel(AlarmModel model){
        mAlarmModel = model;
        displayAlarmInfo();
    }

    private void displayAlarmInfo() {
        String alarmTime = String.format("%s", DateUtils.formatDateTime(getContext(),mAlarmModel.getAlarmTime().getTime().getTime(), DateUtils.FORMAT_SHOW_TIME));
        String alarmDays = "";
        if(mAlarmModel.getAlarmDays().size()==0){
            // TODO: Make sure this is only comparing the hours and not the days
            Date alarmDateTime = mAlarmModel.getAlarmTime().getTime();
            Date nowTime = Calendar.getInstance().getTime();
            boolean isToday = alarmDateTime.after(nowTime);
            if(isToday) {
                alarmDays = "Today";
            }
            else{
                alarmDays = "Tomorrow";
            }
        }
        else{
            if(mAlarmModel.getAlarmDays().contains(Calendar.SUNDAY)){
                alarmDays+="Su/";
            }
            if(mAlarmModel.getAlarmDays().contains(Calendar.MONDAY)){
                alarmDays+="M/";
            }
            if(mAlarmModel.getAlarmDays().contains(Calendar.TUESDAY)){
                alarmDays+="Tu/";
            }
            if(mAlarmModel.getAlarmDays().contains(Calendar.WEDNESDAY)){
                alarmDays+="W/";
            }
            if(mAlarmModel.getAlarmDays().contains(Calendar.THURSDAY)){
                alarmDays+="Th/";
            }
            if(mAlarmModel.getAlarmDays().contains(Calendar.FRIDAY)){
                alarmDays+="F/";
            }
            if(mAlarmModel.getAlarmDays().contains(Calendar.SATURDAY)){
                alarmDays+="Sa/";
            }
            alarmDays=alarmDays.substring(0, alarmDays.length()-1);
        }
        mTextAlarmDefinitionInfo.setText(alarmTime + " " + alarmDays);
        mTextAlarmInstanceInfo.setText(mAlarmModel.getAlarmSound());
        mSwitchAlarmEnable.setChecked(mAlarmModel.getEnabled());
    }

    public void toggleEnable(boolean isChecked) {
        // Set alarm to false if alarm model is not set. Possibly alarmView should be deleted
        // entirely if this is the case
        if(mAlarmModel==null) {
            mSwitchAlarmEnable.setChecked(false);
            return;
        }
        mAlarmModel.setEnabled(isChecked);
    }

    public void doAlarmEditorDialog(){
        // Do nothing if not called from an activity context
        if(!(getContext() instanceof AppCompatActivity)){
            return;
        }
        AlarmEditorDialogFragment dialog = new AlarmEditorDialogFragment(mAlarmModel);
        FragmentManager fragmentManager = ((AppCompatActivity) getContext()).getSupportFragmentManager();
        dialog.show(fragmentManager, "ALARM EDITOR DIALOG");
    }
}
