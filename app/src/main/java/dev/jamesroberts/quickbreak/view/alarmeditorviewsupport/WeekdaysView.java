package dev.jamesroberts.quickbreak.view.alarmeditorviewsupport;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import dev.jamesroberts.quickbreak.R;

/**
 * Section of the Alarm Editor for selecting the number of times the alarm will play.
 *
 * Created by James Roberts, 1-Dec-2020
 */
public class WeekdaysView extends LinearLayout {

    private ToggleButton mToggleSunday;
    private ToggleButton mToggleMonday;
    private ToggleButton mToggleTuesday;
    private ToggleButton mToggleWednesday;
    private ToggleButton mToggleThursday;
    private ToggleButton mToggleFriday;
    private ToggleButton mToggleSaturday;

    public WeekdaysView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutInflater.inflate(R.layout.view_weekdays, this);
        mToggleSunday = findViewById(R.id.toggle_frequency_details_sunday);
        mToggleMonday = findViewById(R.id.toggle_frequency_details_monday);
        mToggleTuesday = findViewById(R.id.toggle_frequency_details_tuesday);
        mToggleWednesday = findViewById(R.id.toggle_frequency_details_wednesday);
        mToggleThursday = findViewById(R.id.toggle_frequency_details_thursday);
        mToggleFriday = findViewById(R.id.toggle_frequency_details_friday);
        mToggleSaturday = findViewById(R.id.toggle_frequency_details_saturday);
    }

    // This is not the most performant way to handle this but it is one of the most readable,
    // and for what this is doing the performance difference is negligible
    public List<Integer> getDays() {
        List<Integer> daysOfWeek = new ArrayList<Integer>();
        if (mToggleSunday.isChecked()) daysOfWeek.add(Calendar.SUNDAY);
        if (mToggleMonday.isChecked()) daysOfWeek.add(Calendar.MONDAY);
        if (mToggleTuesday.isChecked()) daysOfWeek.add(Calendar.TUESDAY);
        if (mToggleWednesday.isChecked()) daysOfWeek.add(Calendar.WEDNESDAY);
        if (mToggleThursday.isChecked()) daysOfWeek.add(Calendar.THURSDAY);
        if (mToggleFriday.isChecked()) daysOfWeek.add(Calendar.FRIDAY);
        if (mToggleSaturday.isChecked()) daysOfWeek.add(Calendar.SATURDAY);
        return daysOfWeek;
    }

    public void setDays(List<Integer> daysOfWeek) {
        mToggleSunday.setChecked(daysOfWeek.contains(Calendar.SUNDAY));
        mToggleMonday.setChecked(daysOfWeek.contains(Calendar.MONDAY));
        mToggleTuesday.setChecked(daysOfWeek.contains(Calendar.TUESDAY));
        mToggleWednesday.setChecked(daysOfWeek.contains(Calendar.WEDNESDAY));
        mToggleThursday.setChecked(daysOfWeek.contains(Calendar.THURSDAY));
        mToggleFriday.setChecked(daysOfWeek.contains(Calendar.FRIDAY));
        mToggleSaturday.setChecked(daysOfWeek.contains(Calendar.SATURDAY));
    }
}
