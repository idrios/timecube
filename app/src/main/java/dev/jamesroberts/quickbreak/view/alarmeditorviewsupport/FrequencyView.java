package dev.jamesroberts.quickbreak.view.alarmeditorviewsupport;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.Calendar;
import java.util.GregorianCalendar;

import dev.jamesroberts.quickbreak.R;
import dev.jamesroberts.quickbreak.model.FrequencyType;

/**
 * Section of the Alarm Editor for inputting what days the alarm will play and how many minutes or
 * hours between each alarm
 *
 * Created by James Roberts, 1-Dec-2020
 */
public class FrequencyView extends LinearLayout {

    private RadioButton mToggleOnce;
    private RadioButton mToggleLimited;
    private RadioButton mToggleUnlimited;

    private EditText mEditOccurrencesTotal;
    private TextView mTextOccurrencesRemaining;

    private EditText mEditFrequencyHour;
    private EditText mEditFrequencyMinute;

    public FrequencyView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutInflater.inflate(R.layout.view_frequency, this);
        mToggleOnce = findViewById(R.id.radio_frequency_once);
        mToggleLimited = findViewById(R.id.radio_frequency_limited);
        mToggleUnlimited = findViewById(R.id.radio_frequency_unlimited);

        mEditOccurrencesTotal = findViewById(R.id.edit_occurrences_total);
        mTextOccurrencesRemaining = findViewById(R.id.text_occurrences_remaining);

        mEditFrequencyHour = findViewById(R.id.edit_frequency_hour);
        mEditFrequencyMinute = findViewById(R.id.edit_frequency_minute);

        mToggleOnce.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    mTextOccurrencesRemaining.setVisibility(INVISIBLE);
                }
            }
        });
        mToggleLimited.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    mTextOccurrencesRemaining.setVisibility(VISIBLE);
                }
            }
        });
        mToggleUnlimited.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    mTextOccurrencesRemaining.setVisibility(INVISIBLE);
                }
            }
        });
    }

    public FrequencyType getFrequencyType() {
        if(mToggleOnce.isChecked()){
            return FrequencyType.ONCE;
        };
        if(mToggleLimited.isChecked()){
            return FrequencyType.LIMITED;
        }
        if(mToggleUnlimited.isChecked()){
            return FrequencyType.UNLIMITED;
        }
        return FrequencyType.NEVER;
    }

    public void setFrequencyType(FrequencyType frequencyType){
        switch (frequencyType){
            case ONCE:
                mToggleOnce.setChecked(true);
                break;
            case LIMITED:
                mToggleLimited.setChecked(true);
                break;
            case UNLIMITED:
                mToggleUnlimited.setChecked(true);
                break;
            default:
                mToggleOnce.setChecked(true);
                break;
        }
    }

    public Calendar getFrequency() {
        Calendar frequency = new GregorianCalendar();
        int hour = 0;
        int minute = 0;
        try {
            hour = Integer.parseInt(mEditFrequencyHour.getText().toString());
        }
        catch (Exception e) {} // consume this
        try{
            minute = Integer.parseInt(mEditFrequencyMinute.getText().toString());
        }
        catch (Exception e){}  // consume this
        int minReal = minute % 60;
        int hourReal = hour + ((minute - minReal)/60);
        if(hourReal > 11){  // Maximum recurrence frequency 11hr 59min
            hourReal = 11;
            minReal = 59;
        }
        if(hourReal <1 && minReal < 1){ // Minimum recurrence frequency 1min
            hourReal = 0;
            minReal = 1;
        }
        frequency.set(Calendar.HOUR, hourReal);
        frequency.set(Calendar.MINUTE, minReal);

        return frequency;
    }

    public void setFrequency(Calendar frequency) {
        mEditFrequencyHour.setText(String.valueOf(frequency.get(Calendar.HOUR)));
        mEditFrequencyMinute.setText(String.valueOf(frequency.get(Calendar.MINUTE)));
    }

    public int getOccurrencesTotal() {
        try {
            return Integer.parseInt(mEditOccurrencesTotal.getText().toString());
        }
        catch (Exception e) {
            return 0;
        }
    }

    public void setOccurrencesTotal(int occurrencesTotal){
        mEditOccurrencesTotal.setText(String.valueOf(occurrencesTotal));
    }

    public int getOccurrencesRemaining() {
        return Integer.parseInt(mTextOccurrencesRemaining.getText().toString());
    }

    public void setOccurrencesRemaining(int occurrencesRemaining) {
        mTextOccurrencesRemaining.setText("("+occurrencesRemaining+") Remaining");
        if(mToggleLimited.isChecked()){
            mTextOccurrencesRemaining.setVisibility(View.VISIBLE);
        }
        else {
            mTextOccurrencesRemaining.setVisibility(View.INVISIBLE);
        }
    }
}
