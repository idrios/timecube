package dev.jamesroberts.quickbreak.view.alarmeditorviewsupport.clockviewsupport;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Just a collection object that has the numbers 0 to 59 if it's the minute, 1 to 12 if it's the
 * hour, or am/pm.
 *
 * This is the equivalent of the shop class in
 * https://github.com/yarolegovich/DiscreteScrollView/blob/master/sample/src/main/java/com/yarolegovich/discretescrollview/sample/shop/Shop.java
 *
 * Created by James Roberts, 27-Nov-2020
 */
public class TimeCollection extends ArrayList<TimeElement> {
    public TimeCollection(String[] timeInputs){
        super();
        for(String time : timeInputs) {
            add(new TimeElement(time));
        }
    }

    public TimeElement get(String timeInput) {
        Iterator<TimeElement> iterator = iterator();
        while(iterator.hasNext()){
            TimeElement element = iterator.next();
            if(element.getValue().equals(timeInput)) {
                return element;
            }
        }
        return null;
    }
}