package dev.jamesroberts.quickbreak.view.alarmeditorviewsupport.clockviewsupport;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import androidx.annotation.Nullable;

import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.InfiniteScrollAdapter;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import dev.jamesroberts.quickbreak.R;

/**
 * TimeView is the View for selecting time on the UI.
 *
 * It uses the DiscreteScrollView to let the user pick hour, minute, or am/pm. It is the equivalent
 * of shop activity in
 * https://github.com/yarolegovich/DiscreteScrollView/blob/master/sample/src/main/java/com/yarolegovich/discretescrollview/sample/shop/ShopActivity.java
 * with minor changes because this is a view and not a full activity
 *
 * Created by James Roberts, 27-Nov-2020
 */
public class TimeView extends FrameLayout implements DiscreteScrollView.OnItemChangedListener<TimeCollectionAdapter.ViewHolder> {

    private TimeElement mCurrentTime;
    private TimeCollection mTimeCollection;
    private DiscreteScrollView mTimePicker;
    private InfiniteScrollAdapter<?> mInfiniteAdapter;
    private boolean mInitialized;

    public TimeView(Context context) {
        super(context);
    }

    public TimeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutInflater.inflate(R.layout.view_time, this);
    }

    @Override
    public void onCurrentItemChanged(@Nullable TimeCollectionAdapter.ViewHolder viewHolder, int adapterPosition) {
        int positionInDataSet = mInfiniteAdapter.getRealPosition(adapterPosition);
        onItemChanged(mTimeCollection.get(positionInDataSet));
    }

    private void onItemChanged(TimeElement time) {
        mCurrentTime = time;
    }

    public void loadValues(String[] times){
        if(mInitialized) {
            return;
        }
        mTimeCollection = new TimeCollection(times);
        mTimePicker = findViewById(R.id.time_picker);
        mTimePicker.addOnItemChangedListener(this);
        mInfiniteAdapter = InfiniteScrollAdapter.wrap(new TimeCollectionAdapter(mTimeCollection));
        mTimePicker.setAdapter(mInfiniteAdapter);
        mTimePicker.setItemTransitionTimeMillis(100);
        mTimePicker.setItemTransformer(new ScaleTransformer.Builder()
                .setMinScale(0.8f)
                .build());
        mTimePicker.setSlideOnFling(true); // enable fling
        onItemChanged(mTimeCollection.get(0));
        mInitialized = true;
    }

    public void setToValue(String time) {
        if(!mInitialized){
            return;
        }
        TimeElement timeElement = mTimeCollection.get(time);
        int index = mTimeCollection.indexOf(timeElement);
        mTimePicker.scrollToPosition(mTimePicker.getCurrentItem()+index);
        mCurrentTime = timeElement;
    }

    public String getTime(){
        return this.mCurrentTime.getValue();
    };
}
