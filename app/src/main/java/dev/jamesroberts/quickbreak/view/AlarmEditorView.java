package dev.jamesroberts.quickbreak.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.Calendar;
import java.util.List;

import dev.jamesroberts.quickbreak.R;
import dev.jamesroberts.quickbreak.model.AlarmModel;
import dev.jamesroberts.quickbreak.model.FrequencyType;
import dev.jamesroberts.quickbreak.view.alarmeditorviewsupport.ClockView;
import dev.jamesroberts.quickbreak.view.alarmeditorviewsupport.EnableView;
import dev.jamesroberts.quickbreak.view.alarmeditorviewsupport.FrequencyView;
import dev.jamesroberts.quickbreak.view.alarmeditorviewsupport.SoundView;
import dev.jamesroberts.quickbreak.view.alarmeditorviewsupport.WeekdaysView;

/**
 *
 * Created by James Roberts, 1-Nov-2020
 */
public class AlarmEditorView extends LinearLayout {

    private ClockView mClockView;
    private FrequencyView mFrequencyView;
    private WeekdaysView mWeekdaysView;
    private SoundView mSoundView;
    private EnableView mEnableView;

    public AlarmEditorView(Context context) {
        super(context);
    }

    public AlarmEditorView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public static AlarmEditorView fromXML(Context context, ViewGroup parent) {
        final AlarmEditorView view = (AlarmEditorView) LayoutInflater.from(context).inflate(R.layout.view_alarm_editor, parent, false);
        view.mClockView = view.findViewById(R.id.clockview);
        view.mFrequencyView = view.findViewById(R.id.frequencyview);
        view.mWeekdaysView = view.findViewById(R.id.weekdaysview);
        view.mSoundView = view.findViewById(R.id.soundview);
        view.mEnableView = view.findViewById(R.id.enableview);
        return view;
    }

    public static AlarmEditorView fromXML(Context context, ViewGroup parent, AlarmModel alarmModel) {
        final AlarmEditorView view = (AlarmEditorView) LayoutInflater.from(context).inflate(R.layout.view_alarm_editor, parent, false);
        view.mClockView = view.findViewById(R.id.clockview);
        view.mFrequencyView = view.findViewById(R.id.frequencyview);
        view.mWeekdaysView = view.findViewById(R.id.weekdaysview);
        view.mSoundView = view.findViewById(R.id.soundview);
        view.mEnableView = view.findViewById(R.id.enableview);

        view.mClockView.setTime(alarmModel.getAlarmTime());
        view.mFrequencyView.setFrequencyType(alarmModel.getAlarmFrequencyType());
        view.mFrequencyView.setFrequency(alarmModel.getAlarmFrequency());
        view.mFrequencyView.setOccurrencesTotal(alarmModel.getAlarmOccurrencesNominal());
        view.mFrequencyView.setOccurrencesRemaining(alarmModel.getAlarmOccurrencesNominal());
        view.mWeekdaysView.setDays(alarmModel.getAlarmDays());
        view.mSoundView.setSound(alarmModel.getAlarmSound());
        view.mEnableView.setEnabled(alarmModel.getEnabled());
        return view;
    }

    public Calendar getTime() {
        return mClockView.getTime();
    };

    public FrequencyType getFrequencyType() {
        return mFrequencyView.getFrequencyType();
    }

    public Calendar getFrequency() {
        return mFrequencyView.getFrequency();
    }

    public List<Integer> getDays() {
        return mWeekdaysView.getDays();
    }

    public int getOccurrencesTotalNominal() {
        return mFrequencyView.getOccurrencesTotal();
    }

    public int getOccurrencesTotalActual() {
        switch (mFrequencyView.getFrequencyType()){
            case ONCE:
                return 1;
            case LIMITED:
                return mFrequencyView.getOccurrencesTotal();
            case UNLIMITED:
                return -1;
            default:
                return 0;
        }
    }

    public String getSound() {
        return mSoundView.getSound();
    }

    public boolean getEnabled() {
        return mEnableView.getEnabled();
    }
}
