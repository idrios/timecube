package dev.jamesroberts.quickbreak.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import dev.jamesroberts.quickbreak.model.AlarmModel;
import dev.jamesroberts.quickbreak.service.AlarmManager;
import dev.jamesroberts.quickbreak.view.AlarmEditorView;

/**
 * A wrapper around AlarmEditorFragment to make it a dialog / modal instead
 * of new page
 */
public class AlarmEditorDialogFragment extends AppCompatDialogFragment {

    private AlarmModel mAlarm;

    public AlarmEditorDialogFragment(AlarmModel alarm){
        mAlarm = alarm;
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final AlarmEditorView alarmEditorView = AlarmEditorView.fromXML(getContext(), null, mAlarm);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Edit Alarm")
                .setView(alarmEditorView)
                .setPositiveButton("confirm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //
                        AlarmManager.getInstance().updateAlarm(new AlarmModel(
                                mAlarm.getId(),
                                alarmEditorView.getTime(),
                                alarmEditorView.getDays(),
                                alarmEditorView.getFrequencyType(),
                                alarmEditorView.getFrequency(),
                                alarmEditorView.getOccurrencesTotalNominal(),
                                alarmEditorView.getOccurrencesTotalActual(),
                                alarmEditorView.getSound(),
                                alarmEditorView.getEnabled()
                                )
                        );
                        alarmEditorView.getTime();
                    }})
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // OnCancel do nothing
                    }});
        return builder.create();

    }
}