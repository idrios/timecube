package dev.jamesroberts.quickbreak.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import dev.jamesroberts.quickbreak.model.AlarmModel;
import dev.jamesroberts.quickbreak.service.AlarmManager;
import dev.jamesroberts.quickbreak.view.AlarmEditorView;

/**
 * A wrapper around AlarmGeneratorFragment to make it a dialog / modal instead
 * of new page
 *
 * Created by James Roberts, 26-Dec-2020
 */
public class AlarmGeneratorDialogFragment extends AppCompatDialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final AlarmEditorView alarmEditorView = AlarmEditorView.fromXML(getContext(), null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Create Alarm")
                .setView(alarmEditorView)
                .setPositiveButton("confirm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //
                        AlarmManager.getInstance().addAlarm( new AlarmModel(
                                        alarmEditorView.getTime(),
                                        alarmEditorView.getDays(),
                                        alarmEditorView.getFrequencyType(),
                                        alarmEditorView.getFrequency(),
                                        alarmEditorView.getOccurrencesTotalNominal(),
                                        alarmEditorView.getOccurrencesTotalActual(),
                                        alarmEditorView.getSound(),
                                        alarmEditorView.getEnabled()
                                )
                        );
                        alarmEditorView.getTime();
                    }})
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // OnCancel do nothing
                    }});
        return builder.create();

    }
}
