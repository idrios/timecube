package dev.jamesroberts.quickbreak.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import dev.jamesroberts.quickbreak.R;
import dev.jamesroberts.quickbreak.view.AlarmListView;

/**
 * Main Menu fragment to view the list of alarms
 */
public class AlarmManagerFragment extends Fragment {

    private AlarmListView mAlarmList;
    private Button mBtnAddAlarm;

    public AlarmManagerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup view = (ViewGroup)inflater.inflate(R.layout.view_alarm_manager, container, false);
        mAlarmList = (AlarmListView) view.findViewById(R.id.layout_alarm_list);
        mAlarmList.loadAlarmListFromMemory();
        mBtnAddAlarm = (Button) view.findViewById(R.id.btn_add_alarm);
        mBtnAddAlarm.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                doAlarmGeneratorDialog();
            }
        });
        return view;
    }

    public void doAlarmGeneratorDialog(){
        // Do nothing if not called from an activity context
        if(!(getContext() instanceof AppCompatActivity)){
            return;
        }
        AlarmGeneratorDialogFragment dialog = new AlarmGeneratorDialogFragment();
        FragmentManager fragmentManager = ((AppCompatActivity) getContext()).getSupportFragmentManager();
        dialog.show(fragmentManager, "ALARM EDITOR DIALOG");
    }
}
