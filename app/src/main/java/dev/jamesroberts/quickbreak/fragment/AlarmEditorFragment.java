package dev.jamesroberts.quickbreak.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import dev.jamesroberts.quickbreak.R;
import dev.jamesroberts.quickbreak.view.alarmeditorviewsupport.ClockView;

/**
 * Fragment for editing alarms
 */
public class AlarmEditorFragment extends Fragment {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_ALARM_ID = "alarmId";
    private String mAlarmId;

    public AlarmEditorFragment() {
        // Required empty public constructor
    }

    /**
     * factory method for creating a new instance using provided parameters
     *
     * @param alarmId Parameter 1.
     * @return A new instance of fragment AlarmEditorFragment.
     */
    public static AlarmEditorFragment newInstance(int alarmId) {
        AlarmEditorFragment fragment = new AlarmEditorFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_ALARM_ID, alarmId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mAlarmId = getArguments().getString(ARG_ALARM_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.view_alarm_editor, container, false);

        final ClockView clockView = view.findViewById(R.id.clockview);
        clockView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clockView.getTime();
            }
        });
        return view;
    }
}