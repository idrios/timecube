package dev.jamesroberts.quickbreak.service;

import java.util.ArrayList;

import dev.jamesroberts.quickbreak.memory.AlarmDaoInternalStorage;
import dev.jamesroberts.quickbreak.memory.AlarmDaoMockInternalStorage;
import dev.jamesroberts.quickbreak.model.AlarmList;
import dev.jamesroberts.quickbreak.model.AlarmModel;

/**
 * AlarmManager manages subscriptions to AlarmEvents. This is a simple implementation -- any
 * observer either subscribes to all or to no events. A better implementation would use generic
 * events, and also allow selective subscriptions to individual kinds of events, but I am trying
 * to just make this work without overcomplicating things (even though this is overcomplicating
 * it already)
 *
 *
 * Created by James Roberts, 2-Jan-2021
 */
public class AlarmManager {
    private ArrayList<AlarmEventObserver> mObservers;
    private static AlarmManager mInstance;
    private AlarmList mList;

    public AlarmManager() {
        mObservers = new ArrayList<>();
        mList = AlarmDaoMockInternalStorage.getInstance().getAllAlarms();
    }

    public static AlarmManager getInstance() {
        if (mInstance == null) {
            mInstance = new AlarmManager();
        }
        return mInstance;
    }

    public void subscribe(AlarmEventObserver observer) {
        mObservers.add(observer);
    }

    public void unsubscribe(AlarmEventObserver observer) {
        mObservers.remove(observer);
    }

    public void addAlarm(AlarmModel alarm){
        mList.add(alarm);
        AlarmDaoInternalStorage.getInstance().addAlarm(alarm);
        if(mObservers != null) {
            for(AlarmEventObserver observer : mObservers){
                observer.onAddAlarm(alarm);
            }
        }
    }

    public void updateAlarm(AlarmModel alarm){
        mList.update(alarm);
        AlarmDaoInternalStorage.getInstance().updateAlarm(alarm.getId(), alarm);
        if(mObservers != null) {
            for(AlarmEventObserver observer : mObservers){
                observer.onUpdateAlarm(alarm);
            }
        }
    }

    public void removeAlarm(AlarmModel alarm){
        mList.remove(alarm);
        AlarmDaoInternalStorage.getInstance().addAlarm(alarm);
        if(mObservers != null) {
            for(AlarmEventObserver observer : mObservers){
                observer.onRemoveAlarm(alarm);
            }
        }
    }

    /**
     * @return a copy of the AlarmList
     */
    public AlarmList getAlarmList() {
        return (AlarmList)this.mList.clone();
    }
}
