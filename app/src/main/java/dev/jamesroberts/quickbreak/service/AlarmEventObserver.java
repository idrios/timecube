package dev.jamesroberts.quickbreak.service;

import dev.jamesroberts.quickbreak.model.AlarmModel;

/**
 * EventObserver interface, using an observer design pattern. AlarmManager manages subscriptions to
 * these events, and any observers (e.g. AlarmListView) listen.
 */
public interface AlarmEventObserver {
    void onAddAlarm(AlarmModel alarm);
    void onUpdateAlarm(AlarmModel alarm);
    void onRemoveAlarm(AlarmModel alarm);
}
