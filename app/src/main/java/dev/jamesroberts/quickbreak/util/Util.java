package dev.jamesroberts.quickbreak.util;

import android.content.res.Resources;

import java.lang.reflect.Field;
import java.util.ArrayList;

import dev.jamesroberts.quickbreak.common.Global;

/**
 * Util for static utility methods that have no dependencies and require no context
 */
public class Util {
    /**
     *
     * @param resources pass getResources() in as an argument here.
     * @param variableName e.g. "alarm_model_8"
     * @param resourceName e.g. "ids"
     * @param packageName probably will be getPackageName()
     * @return
     */
    public static int getResourceId(Resources resources, String variableName, String resourceName, String packageName) {
        try {
            int id = resources.getIdentifier(variableName, resourceName, packageName);
            return id;
        }
        catch (Exception e){
            return -1;
        }
    }
}
