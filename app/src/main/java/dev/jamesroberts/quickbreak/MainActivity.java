package dev.jamesroberts.quickbreak;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import dev.jamesroberts.quickbreak.common.Global;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Instantiate objects
        Global.APPLICATION_CONTEXT = getApplicationContext();
        Global.PACKAGE_NAME = getApplicationContext().getPackageName();
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}