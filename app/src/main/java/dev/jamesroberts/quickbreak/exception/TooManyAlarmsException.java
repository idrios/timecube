package dev.jamesroberts.quickbreak.exception;

/**
 * Created by James Roberts, 26-Dec-2020
 */
public class TooManyAlarmsException extends Exception{
    public TooManyAlarmsException() {
        super("Attempted to create an alarm when the maximum has already been reached");
    }
}
