package dev.jamesroberts.quickbreak.model;

public enum FrequencyType {
    ONCE,
    LIMITED,
    UNLIMITED,
    NEVER
}
