package dev.jamesroberts.quickbreak.model;

import java.util.Calendar;
import java.util.Date;

/**
 * Instance of the Alarm Model.
 *
 * This gets created whenever the alarm is turned on and destroyed when it gets turned off. Any
 * edits (e.g. snooze, number of alarms remaining in a series, etc) are made to the instance
 * instead of the actual (Which is a factory class for making this).
 */
public class AlarmModelInstance {
    private Calendar mAlarmTime;
    private Object mAudioReference; // TODO: How to do audio stuff?

    public AlarmModelInstance(Calendar alarmTime, Object audioReference){
        mAlarmTime = alarmTime;
        mAudioReference = audioReference;
    }

    public Calendar getAlarmTime(){
        return mAlarmTime;
    }

    public Object getAudioReference(){
        return mAudioReference;
    }
}
