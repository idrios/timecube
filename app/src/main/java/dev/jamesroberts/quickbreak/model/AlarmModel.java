package dev.jamesroberts.quickbreak.model;

import android.text.format.DateUtils;
import android.util.Log;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import dev.jamesroberts.quickbreak.service.AlarmManager;

import static android.content.ContentValues.TAG;

/**
 * The data representation of the alarm
 */
public class AlarmModel implements Serializable {
    private final int mId;
    private int mIndex; // Position of alarm in UI
    private boolean mEnabled;
    private Calendar mAlarmTime;
    private List<Integer> mAlarmDays; // Days
    private FrequencyType mAlarmFrequencyType;
    private Calendar mAlarmFrequency; // Hours & Minutes
    private int mAlarmOccurrencesTotalNominal; // Total number of occurrences when frequencyType is "Limited"
    private int mAlarmOccurrencesTotal; // Actual total number of occurrences
    private String mAlarmSound; // TODO: Create or find an audio class

    public AlarmModel() {
        mId = IdCounter.getInstance().generateId();
    }

    public AlarmModel(Calendar alarmTime, List<Integer> alarmDays, FrequencyType alarmFrequencyType, Calendar alarmFrequency, int alarmOccurrencesTotalNominal, int alarmOccurrencesTotal, String alarmSound, boolean enabled) {
        mId = IdCounter.getInstance().generateId();
        mAlarmTime = alarmTime;
        mAlarmDays = alarmDays;
        mAlarmFrequencyType = alarmFrequencyType;
        mAlarmFrequency = alarmFrequency;
        mAlarmOccurrencesTotalNominal = alarmOccurrencesTotalNominal;
        mAlarmOccurrencesTotal = alarmOccurrencesTotal;
        mAlarmSound = alarmSound;
        mEnabled = enabled;
    }

    public AlarmModel(int id, Calendar alarmTime, List<Integer> alarmDays, FrequencyType alarmFrequencyType, Calendar alarmFrequency, int alarmOccurrencesTotalNominal, int alarmOccurrencesTotal, String alarmSound, boolean enabled) {
        mId = id;
        mAlarmTime = alarmTime;
        mAlarmDays = alarmDays;
        mAlarmFrequencyType = alarmFrequencyType;
        mAlarmFrequency = alarmFrequency;
        mAlarmOccurrencesTotalNominal = alarmOccurrencesTotalNominal;
        mAlarmOccurrencesTotal = alarmOccurrencesTotal;
        mAlarmSound = alarmSound;
        mEnabled = enabled;
    }

    public boolean getEnabled() {
        return mEnabled;
    }

    public void setEnabled(boolean enabled) {
        mEnabled = enabled;
        AlarmManager.getInstance().updateAlarm(this);
    }

    public int getId(){
        return mId;
    }

    public int getIndex() {
        return mIndex;
    }

    public void setIndex(int index) {
        mIndex = index;
        AlarmManager.getInstance().updateAlarm(this);
    }

    public Calendar getAlarmTime(){
        return mAlarmTime;
    }

    public void setAlarmTime(Calendar time) {
        mAlarmTime = time;
        AlarmManager.getInstance().updateAlarm(this);
    }

    public List<Integer> getAlarmDays() {
        return mAlarmDays;
    }

    public void setAlarmDays(List<Integer> days) {
        mAlarmDays = days;
        AlarmManager.getInstance().updateAlarm(this);
    }

    public int getAlarmOccurrences() {
        return mAlarmOccurrencesTotal;
    }

    public int getAlarmOccurrencesNominal() {
        return mAlarmOccurrencesTotalNominal;
    }

    public FrequencyType getAlarmFrequencyType() {
        return mAlarmFrequencyType;
    }

    public Calendar getAlarmFrequency() {
        return mAlarmFrequency;
    }

    public String getAlarmSound(){
        return mAlarmSound; // TODO: sound stuff
    }

    public void setAlarmSound(String sound) {
        mAlarmSound = sound; // TODO: sound stuff
        AlarmManager.getInstance().updateAlarm(this);
    }


    public boolean equals(AlarmModel alarmModel) {
        return mId == alarmModel.getId();
    }

    @Override
    public String toString() {
        return "AlarmModel{" +
                "mId=" + mId +
                ", mIndex=" + mIndex +
                ", mEnabled=" + mEnabled +
                '}';
        /*
        return "AlarmModel{" +
                "mId=" + mId +
                ", mIndex=" + mIndex +
                ", mEnabled=" + mEnabled +
                ", mAlarmTime=" + mAlarmTime +
                ", mAlarmFrequency=" + mAlarmFrequency +
                ", mAlarmOccurrences=" + mAlarmOccurrences +
                ", mAlarmSound='" + mAlarmSound + '\'' +
                ", mAlarmInstanceList=" + mAlarmInstanceList +
                '}';
         */
    }
}
