package dev.jamesroberts.quickbreak.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

// The right way is probably to make this class extend a Map or List
public class AlarmList extends ArrayList<AlarmModel> implements Serializable {

    public AlarmList() {
    }

    /**
     * Update an AlarmModel. Note this does not preserve the position of the AlarmModel
     * in the Arraylist
     */
    public void update(AlarmModel alarm) {
        Iterator<AlarmModel> iterator = this.iterator();
        while(iterator.hasNext()){
            AlarmModel alarmModel = iterator.next();
            if(alarmModel.getId() == alarm.getId()){
                iterator.remove();
                this.add(alarm);
                break;
            }
        }
    }

    public boolean equals(AlarmList otherList) {
        throw new UnsupportedOperationException();
    }

    /**
     * Check for the presence of an alarm with the given id. Values need not be identical
     */
    public boolean contains(AlarmModel alarm) {
        Iterator<AlarmModel> iterator = this.iterator();
        while(iterator.hasNext()){
            AlarmModel alarmModel = iterator.next();
            if(alarmModel.getId() == alarm.getId()){
                return true;
            }
        }
        return false;
    }

    /**
     * Get the first AlarmModel found with the given id
     */
    public AlarmModel get(int id) {
        Iterator<AlarmModel> iterator = this.iterator();
        while(iterator.hasNext()){
            AlarmModel alarmModel = iterator.next();
            if(alarmModel.getId() == id){
                return alarmModel;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        if(this.size()==0){
            return "Alarmlist{[]}";
        }
        String out = "AlarmList{[";
        Iterator<AlarmModel> iterator = this.iterator();
        while(iterator.hasNext()){
            AlarmModel alarmModel = iterator.next();
            out+=alarmModel.toString()+", ";
        }
        out=out.substring(0, out.length()-2);
        out+="]}";
        return out;
    }
}
