package dev.jamesroberts.quickbreak.model;

/**
 * Object to keep track of the next alarm id. Created a separate class for this to include
 * load from Memory method and an algorithm to increment the next id. Only requirement is that
 * no 2 id's can ever be equal.
 *
 * use AtomicInteger instead
 */
public class IdCounter {

    public int mValue;
    private static IdCounter mInstance;

    private IdCounter(int initialValue){
        mValue = initialValue;
    }
    public static IdCounter getInstance() {
        if(mInstance == null) {
            mInstance = IdCounter.loadFromMemory();
        }
        return mInstance;
    }

    public int generateId() {
        int vout = mValue;
        incrementCounter();
        return vout;
    }

    private static IdCounter loadFromMemory(){
        return new IdCounter(1);
    }

    private void incrementCounter(){
        mValue++;
    }
}
